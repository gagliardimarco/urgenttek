﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UrgentTek.Interfaces
{
    public interface IWidget
    {
        int PositionX { get; set; }
        int PositionY { get; set; }
        int Diameter { get; set; }
        int Horizontal { get; set; }
        int Vertical { get; set; }
        int Width { get; set; }
        int Height { get; set; }
        string Text { get; set; }

    }
}
