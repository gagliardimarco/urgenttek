﻿namespace UrgentTek.Interfaces
{
    public interface IPositioning
    {
        int PositionX { get; set; }
        int PositionY { get; set; }
    }
}