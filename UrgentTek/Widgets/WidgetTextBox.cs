﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UrgentTek.Builders;
using UrgentTek.Interfaces;

namespace UrgentTek.Widgets
{
    public class WidgetTextBox : WidgetBuilder
    {
        public WidgetTextBox(int x, int y, int height, int widht, string textValue)
        {
            Widget = new Models.Widget();
            Widget.PositionX = x;
            Widget.PositionY = y;
            Widget.Height = height;
            Widget.Width = widht;
            Widget.Text = textValue;
            Widget.WidgetName = "Textbox";

        }

        public override void SetOutput()
        {
            string output = string.Empty;

            output = Widget.WidgetName + " (" + Widget.PositionX + "," + Widget.PositionY + ")";
            output = output + " width=" + Widget.Width + " height=" + Widget.Height + @" text=""" + Widget.Text + @"""";
            Widget.Output = output;
        }


        public override void Validate()
        {
            string output = "+++++Abort+++++";

            bool ok = Widget.PositionX > 0
                && Widget.PositionY > 0
                && Widget.Height > 0
                && Widget.Width > 0;

            if (!ok) Widget.Output = output;

        }



    }
}
