﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UrgentTek.Builders;
using UrgentTek.Interfaces;

namespace UrgentTek.Widgets
{
    public class WidgetEllipse : WidgetBuilder
    {
        public WidgetEllipse(int x, int y, int horizontal, int vertical)
        {
            Widget = new Models.Widget();
            Widget.PositionX = x;
            Widget.PositionY = y;
            Widget.Horizontal = horizontal;
            Widget.Vertical = vertical;
            Widget.WidgetName = "Ellipse";

        }

        public override void SetOutput()
        {
            string output = string.Empty;

            output = Widget.WidgetName + " (" + Widget.PositionX + "," + Widget.PositionY + ")";
            output = output + " diameterH = " + Widget.Horizontal + " diameterV = " + Widget.Vertical;
            Widget.Output = output;
        }


        public override void Validate()
        {
            string output = "+++++Abort+++++";

            bool ok = Widget.PositionX > 0
                && Widget.PositionY > 0
                && Widget.Horizontal > 0
                && Widget.Vertical > 0;

            if (!ok) Widget.Output = output;

        }



    }
}
