﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UrgentTek.Builders;
using UrgentTek.Interfaces;

namespace UrgentTek.Widgets
{
    public class WidgetCircle : WidgetBuilder
    {
        public WidgetCircle(int x, int y, int diameter)
        {
            Widget = new Models.Widget();
            Widget.PositionX = x;
            Widget.PositionY = y;
            Widget.Diameter = diameter;
            Widget.WidgetName = "Circle";

        }

        public override void SetOutput()
        {
            string output = string.Empty;

            output = Widget.WidgetName + " (" + Widget.PositionX + "," + Widget.PositionY + ")";
            output = output + " size=" + Widget.Diameter;
            Widget.Output = output;
        }


        public override void Validate()
        {
            string output = "+++++Abort+++++";

            bool ok = Widget.PositionX > 0
                && Widget.PositionY > 0
                && Widget.Diameter > 0;

            if (!ok) Widget.Output = output;

        }



    }
}
