﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UrgentTek.Builders;
using UrgentTek.Interfaces;

namespace UrgentTek.Widgets
{
    public class WidgetSquare : WidgetBuilder
    {
        public WidgetSquare(int x, int y, int widht)
        {
            Widget = new Models.Widget();
            Widget.PositionX = x;
            Widget.PositionY = y;
            Widget.Width = widht;
            Widget.WidgetName = "Square";

        }

        public override void SetOutput()
        {
            string output = string.Empty;

            output = Widget.WidgetName + " (" + Widget.PositionX + "," + Widget.PositionY + ")";
            output = output + " size=" + Widget.Width;
            Widget.Output = output;
        }

        public override void Validate()
        {
            string output = "+++++Abort+++++";

            bool ok = Widget.PositionX > 0
                && Widget.PositionY > 0
                && Widget.Width > 0;

            if (!ok) Widget.Output = output;

        }
    }
}
