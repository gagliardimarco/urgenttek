﻿using UrgentTek.Interfaces;

namespace UrgentTek.Models
{
    public class Widget : IWidget
    {

        public string WidgetName { get; set; }
        public string Output { get; set; }

        public int PositionX { get ; set ; }
        public int PositionY { get ; set ; }
        public int Diameter { get ; set ; }
        public int Horizontal { get ; set ; }
        public int Vertical { get ; set ; }
        public int Width { get ; set ; }
        public int Height { get ; set ; }
        public string Text { get ; set ; }

        public void OutputWidget()
        {
            // print widget
        }

    }
}