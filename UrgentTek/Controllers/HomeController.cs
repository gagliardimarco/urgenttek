﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UrgentTek.Directors;
using UrgentTek.Models;
using UrgentTek.Widgets;

namespace UrgentTek.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult CreateBill()
        {
            List<Widget> model = new List<Widget>();

            Widget widget;
            WidgetDirector dir = new WidgetDirector();
            widget = dir.MakeWidget(new WidgetRectangle(10, 10, 30, 40));
            widget.OutputWidget();
            model.Add(widget);

            widget = dir.MakeWidget(new WidgetSquare(15, 30, 35));
            widget.OutputWidget();
            model.Add(widget);

            widget = dir.MakeWidget(new WidgetEllipse(100, 150, 300, 200));
            widget.OutputWidget();
            model.Add(widget);

            widget = dir.MakeWidget(new WidgetCircle(1, 1, 300));
            widget.OutputWidget();
            model.Add(widget);


            widget = dir.MakeWidget(new WidgetTextBox(5, 5, 200, 100, "sample text"));
            widget.OutputWidget();
            model.Add(widget);

            // error one
            widget = dir.MakeWidget(new WidgetRectangle(-10, 10, 30, 40));
            widget.OutputWidget();
            model.Add(widget);





            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
