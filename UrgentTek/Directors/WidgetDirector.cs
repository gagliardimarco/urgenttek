﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UrgentTek.Builders;
using UrgentTek.Models;

namespace UrgentTek.Directors
{
    public class WidgetDirector
    {
        public Widget MakeWidget(WidgetBuilder objWidget)
        {
            objWidget.SetOutput();
            objWidget.Validate();
            return objWidget.GetWidget();
        }
    }
}
