﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UrgentTek.Models;

namespace UrgentTek.Builders
{
    public abstract class WidgetBuilder
    {
        protected Widget Widget;
        public abstract void SetOutput();
        public abstract void Validate();

        public Widget GetWidget()
        {
            return Widget;
        }


    }
}
